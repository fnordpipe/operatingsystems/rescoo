# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

DESCRIPTION="meta package for rescue system rootfs"
HOMEPAGE="https://www.fnordpipe.org"
SRC_URI=""

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="
  net-misc/ssh-authorizedkeys
  sys-boot/syslinux
  sys-fs/cryptsetup
  sys-fs/dosfstools
  sys-fs/e2fsprogs
  sys-fs/lvm2
  sys-fs/mdadm
  sys-kernel/linux-stable
  "
